//Objetos

// 18. Prototypes

// Objeto literal
/* const persona = {
    nombre: 'daniel',
    edad: 28,
    raza: 'elfo',
}

console.log(persona)
console.log(persona.raza)

// Objeto CONSTRUCTOR

function Tarea(nombre, vencimiento) {
    this.nombre = nombre;
    this.vencimiento = vencimiento;

}

// Prototype

Tarea.prototype.mostrarInfo = function () {
    return `La tarea '${this.nombre}' vence el '${this.vencimiento}'`;
}

const tarea1 = new Tarea ('Esquilarme','20/01/2020');
const tarea2 = new Tarea('Aprender React', '20/05/2020');
console.log(tarea1)
console.log(tarea1.mostrarInfo()) */

// ########################################################################################

// 19. Object Destructuring

/* const aprendiendoJS = {
    // version:'ES6+',
    version: {
        nueva: 'ES6',
        anterior: 'ES5'
    },
    frameworks: ['React', 'VueJS', 'AngularJS']
}


// let version = aprendiendoJS.version.anterior;
// let frameworks = aprendiendoJS.frameworks[1];

// console.log(aprendiendoJS)
// console.log(version)
// console.log(frameworks)

// let { version, frameworks } = aprendiendoJS;
// console.log(version);
// console.log(frameworks);

let { nueva } = aprendiendoJS.version;
let { frameworks } = aprendiendoJS;
console.log(nueva);
console.log(frameworks[0]); */

// ########################################################################################

// 20. Object Literal Enhacement

/* const banda = 'Pink Floyd';
const genero = 'Rock Psicodelico';
const canciones = ['Wish You Were Here', 'The Wall', 'Dark Side Of The Moon '];

// forma anterior
// const pink_floyd = {
//     banda: banda,
//     genero: genero,
//     canciones : canciones,
// }

// console.log(pink_floyd);

// forma nueva
const pink_floyd = { banda, genero, canciones };
console.log(pink_floyd) */

// ########################################################################################

// 21. FUNCIONES EN UN OBJETO 

// const persona = {
//     nombre: 'dani',
//     trabajo: 'Desarrollador Web',
//     edad: 300,
//     musica: 'Reggae',

// }

// ########################################################################################

// 23- SPREAD OPERATOR  || --> Obtener array duplicado / clon

// let lenguajes = ['Javascript', 'PHP', 'Python'];
// let frameworks = ['React', 'Laravel', 'Django'];

// // let conjunto = lenguajes.concat(framweworks);
// let conjunto = [...lenguajes,...frameworks]
// let [nuevoArreglo] = [...lenguajes].reverse()

// console.log(conjunto);
// console.log(nuevoArreglo);

// function suma(a,b,c){
//     console.log(a+b+c);

// }

// const numeros = [1,2,3]
// suma(...numeros)


// ########################################################################################

// 24- Filter .find y .reduce y métodos para arreglos  || --> Filtrar array con alguna condición, buscar algo en especifico, sumas totales, promedios etc

// const personas = [
//     { nombre: 'Juan', edad: 300, aprendiendo: 'JS' },
//     { nombre: 'Perico', edad: 15, aprendiendo: 'Laravel' },
//     { nombre: 'Norma', edad: 53, aprendiendo: 'Django' },
//     { nombre: 'Pablo', edad: 300, aprendiendo: 'JS' },
//     { nombre: 'Pepe', edad: 41, aprendiendo: 'Artesania' },
// ]

// console.log(personas)


// // Mayores de 41
// const mayores = personas.filter(persona => {
//     // console.log(persona)
//     return persona.edad > 41;
// })

// console.log(mayores);

// // Que aprende Perico y su  edad

// const Perico = personas.find(persona => {
//     return persona.nombre === "Perico";
// })

// console.log(Perico);
// console.log(`Perico está aprendiendo ${Perico.aprendiendo}`);

// let total = personas.reduce((suma, persona)=>{
//     return suma + persona.edad;
// },0);

// console.log(total);
// console.log(total / personas.length);

// ########################################################################################

// 25- Promises     || --> Checa si algo va por un camino o por otro, y en dado casose puede programar el cath para que nos aporte datos sobre el error 

// const aplicarDescuento = new Promise ((resolve, reject)=>{
//     setTimeout(()=>{
//         // let descuento = false;
//         let descuento = true;

//         if (descuento) {
//             resolve('Descuento aplicado!');
//         } else {
//             reject('No hay descuento');
//         }

//     },2000);
// });

// aplicarDescuento.then(resultado =>{
//     console.log(resultado);
// }).catch(error =>{
//     console.log(error);
// })

// ########################################################################################

// 26- Promises con Ajax

// OPCION 1
// const descargarUsuarios = cantidad =>new Promise((resolve,reject)=>{
//     const api = `https://randomuser.me/api/?results=${cantidad}&nat=eu`;

//     const xhr = new XMLHttpRequest();

//     xhr.open('GET',api,true);

//     xhr.onload = ()=>{
//         if (xhr.status == 200) {
//             resolve(JSON.parse(xhr.responseText).results);
//         } else {
//             reject(Error(xhr.statusText));
//         }
//     }
//     xhr.onerror = (error) => reject(error);

//     xhr.send();
// });

// // console.log(descargarUsuarios(20));

// descargarUsuarios(20)
//     .then(
//         miembros => console.log(miembros),
//         error => console.error(
//             new Error('Hubo un error' + error)
//         )
//     )

// OPCION 2   --> NO FUNCIONA, checar

// const descargarUsuarios = cantidad => new Promise((resolve, reject)=>{
//     $.ajax({
//         url: 'https://randomuser.me/api/',
//         type: 'GET',
//         data: {results: cantidad, nat: 'eu'},
//         success:(response)=>{
//             resolve(JSON.parse(response).results);
//         },
//         error: function(XMLHttpRequest, textStatus, errorThrown) { 
//             reject(`Status -->${textStatus} || Error --> ${errorThrown}`)
//             // alert("Status: " + textStatus); alert("Error: " + errorThrown); 
//         }  
//     })
// });

// descargarUsuarios(20)
// .then(
//     miembros => console.log(miembros),
//     error => console.error(
//         new Error('Hubo un error' + error)
//     )
// )

// ########################################################################################

// 27- Mostrando el resultado en nuestro HTML

    // const descargarUsuarios = cantidad => new Promise((resolve, reject) => {
    // const api = `https://randomuser.me/api/?results=${cantidad}&nat=eu`;

    // const xhr = new XMLHttpRequest();

    // xhr.open('GET', api, true);

    // xhr.onload = () => {
    //     if (xhr.status == 200) {
    //         resolve(JSON.parse(xhr.responseText).results);
    //     } else {
    //         reject(Error(xhr.statusText));
    //     }
    // }
    // xhr.onerror = (error) => reject(error);

    // xhr.send();
    // });

    // // console.log(descargarUsuarios(20));

    // descargarUsuarios(20)
    //     .then(
    //         miembros => imprimirHTML(miembros),
    //         error => console.error(
    //             new Error('Hubo un error' + error)
    //         )
    //     )


    // function imprimirHTML(usuarios){
    //     let html = ``;
    //     usuarios.forEach(usuario => {
    //         html +=`
    //         <li>
    //             Name: ${usuario.name.first} ${usuario.name.last}
    //             País: ${usuario.nat}
    //             Imagen: 
    //                 <img src="${usuario.picture.medium}">

    //         </li>`;
    //     });

    //     document.querySelector('#app').innerHTML = html;
    // }


// 28- Programación Orientada a Objetos - Clases

// class Tarea{
//     constructor(nombre, prioridad){
//         this.nombre = nombre;
//         this.prioridad = prioridad;
//     }
//     mostrar(){
//         return(`${this.nombre} tiene una prioridad ${this.prioridad}`);
//     }
// }

// let tarea1 = new Tarea('Aprender JS','Alta');
// let tarea2 = new Tarea('Aprender PHP','Alta');
// let tarea3 = new Tarea('Aprender React','Alta');
// let tarea4 = new Tarea('Comer','Alta');


// console.log(tarea1.mostrar());
// console.log(tarea2.mostrar());
// console.log(tarea3.mostrar());
// console.log(tarea4.mostrar());

// ##############################################################################

// 29- Programación Orientada a Objetos - Clases - Heredando de una Clase | --> Unido al punto 28, ya que hereda de la clase padre

// class ComprasPendientes extends Tarea{
//     constructor(nombre,prioridad,cantidad){
//         super(nombre,prioridad);
//         this.cantidad = cantidad;
//     }
// }


// let compra1 = new ComprasPendientes('Jabon','Alta',3);
// let compra2 = new ComprasPendientes('Lechuga','Muy Alta',1);
// let compra3 = new ComprasPendientes('Verduras','Alta',3);
// let compra4 = new ComprasPendientes('Almendras','Media',3);

// console.log(compra1);
// console.log(compra2.mostrar());
// console.log(compra3.mostrar());
// console.log(compra4);

// ##############################################################################


// 30- Módulos en ES6  || --> Explicación sobre como exportar/importar de un archivo js hacía otro en forma de (Va unido al archivo tareas.js) 

// Opcion 1 IMPORTAR
// import {nombreTarea} from './tareas.js';

// Opcion 2 IMPORTAR
// import nombreTarea from './tareas.js';

// console.log(nombreTarea);

// ##############################################################################


// 31- Exportando Funciones en Módulos

// import {nombreTarea,crearTarea,tareaCompletada} from './tareas.js';


// const tarea1 = crearTarea('Pasear al perro', 'alta');

// console.log(tarea1);
// tareaCompletada();



// 32- Exportando Clases en Módulos  || --> Lo mismo que en las dos anteriores pero en este caso lo que exportariamos serían CLASES de DIFERENTES ARCHIVOS al archivo deseado.

import Tarea from './tareas.js';

const tarea1 = new Tarea('Comer','Alta');

console.log(tarea1);
tarea1.mostrar();
