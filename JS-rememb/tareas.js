// EXPORTAR VARIABLES

// Opcion 1 para EXPORTAR
export const nombreTarea = 'Pasear al perro';

// Opcion 2 para EXPORTAR  --> Con VARIABLE por DEFECTO
// const nombreTarea = 'Pasear al perro';
// export default nombreTarea;

// Opcion 3 para EXPORTAR  --> Con VARIABLE por DEFECTO, en forma de OBJETO
// const nombreTarea = 'Pasear al perro';
// const fecha = 'Hoy';
// export default {
//     nombre: nombreTarea,
//     fecha: fecha,
// };

export const crearTarea = (tarea, urgencia) => {
    return `La tarea ${tarea} tiene una urgencia ${urgencia} `
}

export const tareaCompletada = ()=>{
    console.log(`La tarea se completo!`)
}


// ESCRIBIR CLASES

export default class Tarea {
    constructor(nombre, prioridad) {
        this.nombre = nombre;
        this.prioridad = prioridad;
    }
    mostrar() {
        console.log (`${this.nombre} tiene una prioridad ${this.prioridad}`);
    }
}

let tarea1 = new Tarea('Aprender JS', 'Alta');
let tarea2 = new Tarea('Aprender PHP', 'Alta');
let tarea3 = new Tarea('Aprender React', 'Alta');
let tarea4 = new Tarea('Comer', 'Alta');


// console.log(tarea1.mostrar());
// console.log(tarea2.mostrar());
// console.log(tarea3.mostrar());
// console.log(tarea4.mostrar());




// class ComprasPendientes extends Tarea {
//     constructor(nombre, prioridad, cantidad) {
//         super(nombre, prioridad);
//         this.cantidad = cantidad;
//     }
// }


// let compra1 = new ComprasPendientes('Jabon', 'Alta', 3);
// let compra2 = new ComprasPendientes('Lechuga', 'Muy Alta', 1);
// let compra3 = new ComprasPendientes('Verduras', 'Alta', 3);
// let compra4 = new ComprasPendientes('Almendras', 'Media', 3);

    // console.log(compra1);
    // console.log(compra2.mostrar());
    // console.log(compra3.mostrar());
    // console.log(compra4);