import React from 'react';

const Producto = ({ producto, carrito, agregarProducto, productos }) => {

    const { nombre, precio, id } = producto

    // Agregar producto al carrito
    const seleccionarProducto = (id) => {
        const producto = productos.filter(producto => (
            producto.id === id
        ))
        // console.log(producto[0])
        agregarProducto([
            ...carrito, ...producto
        ])
        // console.log('Comprando...', id);
        // console.log('Comprando...', nombre);
    }


    // Eliminamos un producto del carrito
    const eliminarProducto = id => {
        const productos = carrito.filter(producto => producto.id !== id);
        agregarProducto(productos)
    }


    return (
        <div>
            {/* <h2>{producto.nombre}</h2> */}
            <h2>{nombre} - ${precio}</h2>
            {productos
                ? (
                    <button
                        type="button"
                        onClick={() => seleccionarProducto(id)}
                    // onClick = {() => seleccionarProducto(nombre)}
                    >Comprar</button>
                )
                : (
                    <button
                        type="button"
                        onClick={() => eliminarProducto(id)}
                    // onClick = {() => seleccionarProducto(nombre)}
                    >Eliminar</button>
                )
            }
        </div>
    );
}
export default Producto;