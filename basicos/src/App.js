import React, {Fragment, useState} from 'react';
import logo from './logo.svg';
import './App.css';
import Header from './componentes/Header';
import Footer from './componentes/Footer';
import Producto from './componentes/Producto';
import Carrito from './componentes/Carrito';


function App() {


  // Crear listado de productos 
  const [productos, setProductos] = useState([
    {id:1, nombre:'Camisa ReactJS', precio: 50},
    {id:2, nombre:'Pantalon ReactJS', precio: 30},
    {id:3, nombre:'Short ReactJS', precio: 9},
    {id:4, nombre:'Sport ReactJS', precio: 12},
  ]);

  // State para un carrito de compras
  const [ carrito, agregarProducto] = useState([]);

  // Obtenemos la fecha 
  const fecha = new Date().getFullYear();
  return (
    // <div className="App">
      <Fragment>
        <Header 
        titulo = 'Tienda Virtual' 
        numero = {20}
        />
        <h1>Lista de Productos</h1>
        {productos.map(producto=>(
          <Producto 
          key= {producto.id}
          producto = {producto}
          productos = {productos}
          carrito = {carrito}
          agregarProducto = {agregarProducto}
          />
        ))}
        
        <Carrito 
        carrito = {carrito}
        agregarProducto = {agregarProducto}
        />
        <Footer 
        fecha = {fecha}
        />
      </Fragment>
    // </div>
      /* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1>Hola Mundo</h1>
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> */
      
  );
}

export default App;
